let express = require("express");
let bodyParser = require("body-parser");
let cors = require("cors");

let app = express();
let PORT = process.env.PORT || 8080;

require("./app/Config/Global");

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(__dirname + "/public"));

//ROUTING
let taskRouter = require("./app/Routes/Task");
app.use("/task", taskRouter);

app.get("/", function (req, res) {
  res.send("TODO APP API.");
});

app.listen(PORT, function () {
  console.log("Listing on " + PORT);
});
