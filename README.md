## Application Functionality

Todo app API:

- Setup the node js project with express and sequelize.
- Add, edit and list API is created.
- Deployed the app on google cloud
- https://todo-node-28441-6aaqqpo7fq-uc.a.run.app/
- app.yaml file is used for the cloud configuration
- Dockerfile is used for the setting up the container

## Available Scripts

### `npm install`

To install packages

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

### `API end point`

- cloud: https://todo-node-28441-6aaqqpo7fq-uc.a.run.app/

- POST: {{cloud}}task/add
- PUT: {{cloud}}task/edit
- GET: {{cloud}}task/list
- DELETE: {{local}}task/delete/:id
