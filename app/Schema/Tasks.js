let tasks = database.define("tasks", {
  task: {
    type: Sequelize.STRING,
  },
  is_completed: {
    type: Sequelize.BOOLEAN,
    defaultValue: 0,
  },
});

tasks.sync({
  logging: false,
});

module.exports = tasks;
