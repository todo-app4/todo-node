let randomstring = require("randomstring");
let bcrypt = require("bcrypt-nodejs");
let saltRounds = 10;

class TokenGenerator {
  generator() {
    let tokenString = randomstring.generate(25);
    let token = bcrypt.hashSync(tokenString, bcrypt.genSaltSync(10), null);
    return token;
  }
}

module.exports = TokenGenerator;
