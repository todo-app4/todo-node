exports.DATABASE = {
  HOST: `/cloudsql/${process.env.INST_CON_NAME}`,
  NAME: process.env.SQL_NAME,
  USER: process.env.SQL_USER,
  PASSWORD: process.env.SQL_PASSWORD,
};

exports.EMAIL = {
  SERVICE: "gmail",
  HOST: "smtp.gmail.com",
  PORT: 465,
  USERNAME: "",
  PASSWORD: "",
};
