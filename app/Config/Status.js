exports.CODES = {
  CONFLICT: {
    CODE: 409,
    MESSAGE: "Provided information already exist!",
  },
  FORBIDDEN: {
    CODE: 403,
    MESSAGE: "Forbidden Error !",
  },
  MISSING_AUTHENTICATION_DATA: {
    CODE: 417,
    MESSAGE: "Authentication Information Missing",
  },
  NOT_ALLOWED: {
    CODE: 405,
    MESSAGE: "Method not allowed !",
  },
  NOT_FOUND: {
    CODE: 404,
    MESSAGE: "Requested resource not found !",
  },
  PRE_CONDITION: {
    CODE: 412,
    MESSAGE: "Please complete other steps first",
  },
  SERVER_ERROR: {
    CODE: 500,
    MESSAGE: "Internal Server Error",
  },
  SUCCESS: {
    CODE: 200,
    MESSAGE: "Success",
  },
  RECORD_CREATED: {
    CODE: 201,
    MESSAGE: "Success",
  },
  UNAUTHORIZED: {
    CODE: 401,
    MESSAGE: "You are not authorized to access this location.",
  },
};

exports.MESSAGES = {
  TASK: {
    TASK_ADD: "Task has been added successfully",
    TASK_EDIT: "Task has been edited successfully",
    TASK_delete: "Task has been delete successfully",
  },
};
