let express = require("express");
let router = express.Router();

let TasksController = require("../Controllers/API/TasksController");
let tasksController = new TasksController();

router.route("/list").get(tasksController.list);
router.route("/add").post(tasksController.add);
router.route("/edit").put(tasksController.edit);
router.route("/delete/:id").delete(tasksController.delete);

module.exports = router;
