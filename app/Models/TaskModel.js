let Status = require("../Config/Status");
let TasksSchema = require("../Schema/Tasks");

class TaskModel {
  constructor() {
    console.log("TaskModel");
    this.tasksSchema = TasksSchema;
  }

  list() {
    return new Promise((resolve, reject) => {
      this.tasksSchema
        .findAll({
          order: [["created_at", "ASC"]],
        })
        .then((tasks) => {
          resolve({
            status: Status.CODES.SUCCESS.CODE,
            message: Status.CODES.SUCCESS.MESSAGE,
            data: tasks,
          });
        })
        .catch((error) => {
          reject({
            status: Status.CODES.SERVER_ERROR.CODE,
            message: Status.CODES.SERVER_ERROR.MESSAGE,
            error: error,
          });
        });
    });
  }

  add(res, data) {
    return new Promise((resolve, reject) => {
      this.tasksSchema
        .create(data)
        .then((task) => {
          resolve({
            status: Status.CODES.RECORD_CREATED.CODE,
            message: Status.MESSAGES.TASK.TASK_ADD,
            data: task.dataValues,
          });
        })
        .catch((error) => {
          reject({
            status: Status.CODES.SERVER_ERROR.CODE,
            message: Status.CODES.SERVER_ERROR.MESSAGE,
            error: error,
          });
        });
    });
  }

  edit(res, data) {
    return new Promise((resolve, reject) => {
      this.tasksSchema
        .update(data, {
          where: {
            id: data.id,
          },
        })
        .then((result) => {
          console.log("TaskModel -> edit -> result", result);
          resolve({
            status: Status.CODES.RECORD_CREATED.CODE,
            message: Status.MESSAGES.TASK.TASK_EDIT,
            data,
          });
        })
        .catch((error) => {
          reject({
            status: Status.CODES.SERVER_ERROR.CODE,
            message: Status.CODES.SERVER_ERROR.MESSAGE,
            error: error,
          });
        });
    });
  }

  delete(req, res) {
    return new Promise((resolve, reject) => {
      this.tasksSchema
        .destroy({
          where: {
            id: req.params.id,
          },
        })
        .then(() => {
          resolve({
            status: Status.CODES.SUCCESS.CODE,
            message: Status.CODES.SUCCESS.MESSAGE,
          });
        })
        .catch((error) => {
          reject({
            status: Status.CODES.SERVER_ERROR.CODE,
            message: Status.CODES.SERVER_ERROR.MESSAGE,
            error: error,
          });
        });
    });
  }
}

module.exports = TaskModel;
