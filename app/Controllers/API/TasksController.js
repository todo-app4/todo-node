let TaskModel = require("../../Models/TaskModel");
let taskModel = new TaskModel();

class TasksController {
  constructor() {
    console.log("TasksController");
  }

  add(req, res) {
    taskModel
      .add(res, req.body)
      .then((data) => {
        res.send(data);
      })
      .catch((error) => {
        console.log(error);
        res.send(error);
      });
  }

  edit(req, res) {
    taskModel
      .edit(res, req.body)
      .then((data) => {
        res.send(data);
      })
      .catch((error) => {
        console.log(error);
        res.send(error);
      });
  }

  delete(req, res) {
    console.log("TasksController -> delete -> req.params.id", req.params.id);
    taskModel
      .delete(req, res)
      .then((data) => {
        res.send(data);
      })
      .catch((error) => {
        console.log(error);
        res.send(error);
      });
  }

  list(req, res) {
    taskModel
      .list()
      .then((data) => {
        res.send(data);
      })
      .catch((error) => {
        console.log(error);
        res.send(error);
      });
  }
}

module.exports = TasksController;
